/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    private CommandWords commandWords;
    private Player player;
    public Item pilsje;
    public Item useless_rock;
    public Item proton_core;
    public static boolean grabPils;
    public boolean usedPils;
    public boolean pilsAanwezig;
    public NPC worthless;
    public Room asteroid, spaceship, earth, mars, nebula;
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        player = new Player();
        createRooms();
        parser = new Parser();
        commandWords = new CommandWords();
        grabPils = false;
        usedPils = false;
        pilsAanwezig = true;
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        //NPC worthless;
        // create the rooms
        asteroid = new Room("asteroid", "You appear to be on a rock in space");
        spaceship = new Room("spaceship", "in your ship");
        earth = new Room("earth", "Back on earth. You remember this place");
        mars = new Room("mars", "on some foreign planet. Looks weird");
        nebula = new Room("nebula", "In a place you have never been before for sure");

        // initialise room exits
        asteroid.setExit("west", spaceship);

        spaceship.setExit("east", asteroid);
        spaceship.setExit("south", earth);
        spaceship.setExit("west", mars);

        mars.setExit("east", spaceship);
        mars.setExit("south", nebula);

        nebula.setExit("north", mars);

        earth.setExit("north", spaceship);

        //initialise items
        pilsje = new Item("pilsje", "a green bat.", asteroid);
        useless_rock = new Item("UselessRock", "I don't see why you would even want that.", asteroid);
        proton_core = new Item("ProtonCore", "Seems like a game changer", mars);

        //place items
        asteroid.setItem("pilsje", pilsje);
        asteroid.setItem("uselessRock", useless_rock);
        
        //initialise NPC's
        worthless = new NPC("worthless", "doesn't seem to move much.", 20, mars);

        //place player
        player.setCurrentRoom(asteroid);  // start game outside
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.

        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("Welcome to the World of Space V1!");
        System.out.println("You wake up and have no idea where you are.");
        System.out.println("Last night must've been great.");
        System.out.println("Type '" + CommandWord.HELP + "' to go over your options.");
        System.out.println();
        System.out.println(player.getCurrentRoom().getLongDescription());
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;

        CommandWord commandWord = command.getCommandWord();

        switch (commandWord) {
            case UNKNOWN:
            System.out.println("I don't know what you mean...");
            break;

            case HELP:
            printHelp();
            break;

            case GO:
            goRoom(command);
            break;

            case BACK:
            goBack();
            break;

            case QUIT:
            wantToQuit = quit(command);
            break;

            case LOOK:
            printLocationInfo();
            break;

            case GRAB:
            grabItem(command);
            break;

            case PLAYERINFO:
            player.playerInfo();
            break;
        }
        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are lost. You are alone.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println(commandWords.showAll());
    }

    /** 
     * Try to go in one direction. If there is an exit, enter the new
     * room, otherwise print an error message.
     */
    private void goRoom(Command command){
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();
        if(player.getCurrentRoom() == spaceship && grabPils == true){
            System.out.println("You finish your pilsje and move on, and as you take the last sip");
            System.out.println("you feel the empty bottle fade from your inventory...");
            grabPils = false;
            usedPils = true;
        }
        else if(command.getSecondWord().equals("west") && player.getCurrentRoom().getName().equals("asteroid")){
            if(usedPils == true){
                player.goRoom(direction);
                return;
            }
            else if(grabPils == false){
                System.out.println("You are to thirsty to do anything right now.");
                return;
            }
            else{
                System.out.println("You are refreshed enough to hop back onto your ship.");
                player.goRoom(direction);
                return;
            }
        }
        player.goRoom(direction);

    }

    /**
     * "look" was entered.
     * Get information on where you are.
     */
    private void printLocationInfo()//look
    {
        //System.out.println("You are " + currentRoom.getDescription());
        System.out.println(player.getCurrentRoom().getLongDescription());
        if(player.getCurrentRoom() == asteroid && grabPils == false && pilsAanwezig == true){
            System.out.println("You see you are accompanied by a pilsje, it is within your reach.");
        }
        else if(player.getCurrentRoom() == mars && worthless.getNpcLocation() == mars){
            System.out.println("You spot something, you try to identify it.");
            System.out.println("name: " + worthless.getNpcName());
            System.out.println("description: " + worthless.getNpcDescription());
            System.out.println("hitpoints: " + worthless.getNpcHitpoints());
            System.out.println("As it sees you approach, you hear it mutter 'please just leave, I have nothing...'");
        }
        else if((player.getCurrentRoom() == spaceship && player.prevRoom == mars)){
            worthless.npcLocation = spaceship;
            System.out.println("As you re-enter your ship, you see that you're being followed...");
            System.out.println("'Please don't just leave me there, take me with you!'");
            System.out.println("'I want an adventure aswell!'");
            System.out.println("");
            System.out.println("How did I get myself into this...");
        }
        else if(worthless.npcLocation == spaceship && player.getCurrentRoom() == spaceship){
            System.out.println("'Thank you so much for letting me stay on your ship master!'");
        }
        else{
            System.out.println("Nothing more to see here.");
        }
    }

    /**
     * "back" was entered.
     * player travels back to previous room.
     */
    private void goBack()
    {
        player.goBack();
    }

    /**
     * Pick up the pilsje to feel refreshed,
     * and continue your journey.
     */
    private boolean grabItem(Command command)
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("What do you want to grab?");
        }
        else if(grabPils == false && player.getCurrentRoom().getName().equals("asteroid") && command.getSecondWord().equals("pilsje")){
            grabPils = true;
           System.out.println("You grab the pilsje and drink it.");
            System.out.println("You feel ready to move again.");
            pilsAanwezig = false;
        }
        else{
            System.out.println("Either it doesn't exist, or there's nothing to grab...");
        }

        return grabPils;
    }

    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
