/**
 * This is an NPC to fight.
 */
public class NPC
{
    private Room npcRoom;
    public int npcHitpoints;
    public String npcName;
    public String npcDescription;
    public Room npcLocation;
    /**
     * Constructor for objects of class NPC
     */
    public NPC(String name, String description, int hitpoints, Room room)
    {
        npcName = name;
        npcDescription = description;
        npcHitpoints = hitpoints;
        npcLocation = room;
    }
    
    public String getNpcName()
    {
        return npcName;
    }
    
    public String getNpcDescription()
    {
        return npcDescription;
    }
    
    public int getNpcHitpoints()
    {
        return npcHitpoints;
    }
    
    public Room getNpcLocation()
    {
        return npcLocation;
    }
}