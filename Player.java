
/**
 * Write a description of class Player here.
 */
public class Player
{
    // instance variables - replace the example below with your own
    private Room currentRoom;
    private boolean itemPickup;
    public int playerHitpoints;
    public Room prevRoom;

    /**
     * Constructor for objects of class Player
     */
    public Player()
    {
        playerHitpoints = 100;
        boolean playerItem = itemPickup;
    }
    
    public int getPlayerHitpoints()
    {
        return playerHitpoints;
    }
    
    public Room getCurrentRoom()
    {
        return currentRoom;
    }
    
    /**
     * Set room of player.
     */
    public void setCurrentRoom(Room room)
    {
        currentRoom = room;
    }
    
    public void goRoom(String direction)
    {
        // Try to leave current room.
        Room nextRoom = getCurrentRoom().getExit(direction);
        if (nextRoom == null) {
            System.out.println("There is nothing to explore there...");
        }
        else {
            prevRoom = getCurrentRoom();
            currentRoom = nextRoom;
            System.out.println(getCurrentRoom().getLongDescription());

        }
    
    }
    
    public void goBack()
    {
        if(prevRoom != null){
            currentRoom = prevRoom;
            System.out.println(currentRoom.getLongDescription());
        }

        else{
            System.out.println("You can't travel any further back then where you started.");
        }

    }
    /**
     * Alle info over de player weergeven.
     *
     */
    public void playerInfo()
    {
        System.out.println("Current location: " + getCurrentRoom().getName());
        System.out.println("Your hitpoints: " + getPlayerHitpoints());
        System.out.println("Your Items: ");
        if(Game.grabPils == true){
            System.out.println("1x - Pilsje");
        }
    }
}
