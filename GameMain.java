/**
 * Running the game through powershell.
 *
 * @author Daan van Avezaath 
 * @version 24-11-2017
 */
public class GameMain {
    private String playerName;
    private String args[];

    /**
     * The starting point of the game.
     */
    public static void main(String[] args) {
        if(args.length == 1) {
            System.out.println("Hello" + args[0]);
        }
        Game game = new Game();
        game.play();
    }
}