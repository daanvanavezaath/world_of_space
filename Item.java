import java.lang.String;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Write a description of class Item here.
 * 
 */
public class Item
{
    // instance variables - replace the example below with your own
    private String itemName;
    private String itemDescription;
    private Room itemRoom;

    
    /**
     * Constructor for objects of class Item
     */
    public Item(String name, String description, Room itemRoom)
    {
        itemName = name;
        itemDescription = description;
        this.itemRoom = itemRoom;
    }
}
